const mongoose = require("../db");

fileSchema = mongoose.Schema(
  {
    fileName: {
      type: String,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("files", fileSchema);
