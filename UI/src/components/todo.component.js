import React, { Component } from "react";
import axios from "axios";

export default class Todo extends Component {
  constructor(props) {
    super(props);
    this.state = { todo: [], file: null };
  }

  async componentDidMount() {
    const data = await axios.get("http://localhost:3001/files");
    console.log(data);
    this.setState((this.state.todo = data.data));
  }

  render() {
    return (
      <div>
        <form
          action=""
          className="p-2"
          method="post"
          onSubmit={this.handleSubmit}
        >
          <div className="form-group">
            <input
              type="file"
              name="file"
              onChange={this.handleChange.bind(this)}
              className="form-control"
            />
          </div>
          <button className="btn btn-sm btn-success" type="submit">
            upload
          </button>
        </form>
        <this.RenderList list={this.state.todo}></this.RenderList>;
      </div>
    );
  }

  RenderList(list = []) {
    list = list.list;
    return list.map((_item) => {
      console.log(_item);
      return (
        <div className="card p-2">
          {" "}
          <div>
            <a
              href={"http://localhost:3001/uploads/" + _item.fileName}
              target="_blank"
              rel="noopener noreferrer"
            >
              <img
                src={"http://localhost:3001/uploads/" + _item.fileName}
                alt=""
                width="100"
                height="100"
                className="img-fluid"
              />
            </a>
          </div>{" "}
        </div>
      );
    });
  }

  FileUploadForm() {
    return (
      <form
        action=""
        className="p-2"
        method="post"
        encType="multipart/form-data"
        onSubmit={this.handleSubmit}
      >
        <div className="form-group">
          <input
            type="file"
            name="file"
            onChange={this.handleChange.bind(this)}
            className="form-control"
          />
        </div>
        <button className="btn btn-sm btn-success" type="submit">
          Upload
        </button>
      </form>
    );
  }

  handleChange(event) {
    this.setState((this.state.file = event.target.files[0]));
  }

  handleSubmit = (event) => {
    event.preventDefault();
    this.submitForm();
  };

  submitForm = async () => {
    console.log({ state: this.state });
    const formData = new FormData();
    formData.append("myFile", this.state.file, this.state.file.name);
    const response = await axios.post("http://localhost:3001/files", formData, {
      headers: {
        "content-type": "multipart/form-data",
      },
    });
    var data = response.data;
    if (!data.err) {
      this.setState((this.state.todo = data));
    }
  };
}
